include "../base.thrift"

namespace py device_info
namespace go device_info
namespace java com.bytedance.device.info

# 当出现异常时，会返回具体错误的提示，外部用户可以不关心，主要看 StatusCode 即可
const string STATUS_MSG_SUCCESS = "success"
enum BaseRespStatusCode{
    # https://wiki.bytedance.com/pages/viewpage.action?pageId=52049142
    # ｜－－－－－－－｜－－－－－－－｜－－－－－－－｜－－－－－－－－－｜
    #   8（位）保留    7位（产品线）   6位（模块）   10位（具体错误号）
    #
    # 生成方式:
    # (1 << 16) * 4 | (1 << 10) * 1 | 1
    # 263169
    # (1 << 16) * 4 | (1 << 10) * 1 | 2
    # 263170
    # (1 << 16) * 4 | (1 << 10) * 1 | 3
    # 263171
    SUCCESS = 0,
    FAIL_UNKNOWN = 263169,
    FAIL_DB_ERROR = 263170,
    FAIL_RECORD_NOT_FOUND = 263171,
    FAIL_SERVICE_CIRCUIT_BREAK = 263172,
    FAIL_RECORD_NOT_FOUND_PERHAPS_NOT_SYNCED = 263173,
    FAIL_PARAM_INVALID = 263174,
    FAIL_HOT_DEVICE_ID = 263175,
    FAIL_HOT_INSTALLATION_ID = 263176,
    FAIL_STRESS_NOT_SUPPORTED = 263177,
}

enum InstallationCode{
    	RegularRequest = 0, // 例行请求
    	FirstInstallation = 1, // 首次安装
    	UpgradeInstallation = 2, // 升级
    	ReInstallationWithNewInstallation = 3, // 卸载重装，有新安装
    	ReInstallationWithOutNewInstallation = 4, // 卸载重装，无新安装
}

enum Scene{
    	Common = 0, // 非特殊场景
    	NewUserMode = 1, // 新用户模式
    	Child = 2, // 儿童模式
}


struct Device {
    1: i64 id=0,
    2: string udid (go.tag = "json:\"udid,sensitive\""), // imei for Android in cn, unknow out of cn
    3: string openudid, // android_id for Android, unknow for other os
    4: string clientudid, // abandoned
    5: string idfa,
    6: string model,
    7: string os,
    8: string os_version,
    9: i32 os_api,
    10: string resolution,
    11: string display_density,
    12: bool is_jailbroken,
    13: string mc (go.tag = "json:\"mc,sensitive\""), // mac for Android in cn, empty out of cn
    14: string carrier,
    15: i64 create_time,
    16: i64 modify_time=0,
    17: string rom,
    18: string aliyun_uuid,
    19: string language,
    20: string region,
    21: string tz_name,
    22: i32 tz_offset,
    23: string build_serial,
    24: optional string pc_uuid,
    25: optional string pc_serial,
    26: optional string macos_uuid,
    27: optional string macos_serial,
    28: optional i32 cheat_code,
    29: optional string oaid,
    30: optional string device_brand,
    31: optional string drm_id,
    32: optional string drm_provider,
    35: optional bool is_track_limited,
    36: optional string corrected_tag, // corrected_tag有值就表示矫正过了，具体的值代表矫正的版本
    37: optional string google_aid,

    // 38~44: iOS global did, [doc link](https://bytedance.feishu.cn/docs/doccn7XY6TzHhvBDrgisugza9Pc)，
    // Services beyond UCenter/UCenter/Device, will recieve empty fields below.
    38: optional string local_tz_name,
    39: optional string startup_time,
    40: optional string mb_time,
    41: optional string phone_name,
    42: optional string cpu_num,
    43: optional string disk_total,
    44: optional string mem_total,
    45: optional i32 auth_status,

    46: optional Scene scene, // 表示这个设备是什么场景下创建的

    47: optional string tizen_duid, // https://developer.samsung.com/smarttv/develop/api-references/samsung-product-api-references/productinfo-api.html#::ProductInfo::ProductInfoManager::getDuid
    48: optional string tizen_uuid, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/uuid.html
    49: optional string tizen_tifa, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/tizen-id-for-advertising.html

    50: optional string build_time,  // for global did
    51: optional string hardware_model,
    52: optional string locale_language,
    53: optional string ct_uuid,

    54: optional string webos_uuid, 

    55: optional string app_trait, 
    56: optional bool is_harmony,  // Set true if it belongs to Huawei Harmony OS.
    57: optional string pico_sn,
}

struct GetDeviceRequest {
    1: required i64 did,
    2: required base.Base Base,
}

struct GetDeviceByMarksRequest {
    1: optional string udid,
    2: optional string clientudid, // ios vendor_id
    3: optional string openudid,
    4: optional string mc,
    5: optional string idfa,
    6: required string os, // ios, android
    7: optional string os_version, // 9.1, 9.1.0
    8: required base.Base Base,
}

struct GetDeviceByIdentifiersRequest {
    1: optional string udid (go.tag = "json:\"udid,sensitive\""),
    2: optional string clientudid, // ios vendor_id
    3: optional string openudid,
    4: optional string mc (go.tag = "json:\"mc,sensitive\""),
    5: optional string idfa,
    6: required string os, // ios, android
    7: optional string os_version, // 9.1, 9.1.0
    8: optional string build_serial,
    9: optional string google_aid,
    10: optional string pc_uuid,
    11: optional string pc_serial,
    12: optional string macos_uuid,
    13: optional string macos_serial,
    14: optional string model,
    15: optional string oaid, // for android, from android Q
    16: optional string device_brand, // for oaid matcher
    17: optional string sub_os, // 比如：os是Android，sub_os是TV，就表示使用TV规则查找设备

    18: optional string tizen_duid, // https://developer.samsung.com/smarttv/develop/api-references/samsung-product-api-references/productinfo-api.html#::ProductInfo::ProductInfoManager::getDuid
    19: optional string tizen_uuid, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/uuid.html
    20: optional string tizen_tifa, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/tizen-id-for-advertising.html

    21: optional string webos_uuid, 

    22: optional string app_trait, 
    23: optional string pico_sn,

    255: required base.Base Base,
}

struct CASDeviceRequest {
    1: required Device device,
    2: optional string ip,
    3: optional string user_agent,
    4: optional string channel,
    5: optional i32    sdk_target_version,
    6: optional string sdk_version,
    7: optional string rule_version,
    255: required base.Base Base,
}

// update fields: is_jailbroken, **clientudid, vendor_id**, carrier, os_version, os_api, idfa
// Deprecated 弃用
struct UpdateDeviceRequest {
    1: required i64 did,
    2: optional string clientudid,
    3: optional string os_version,
    4: optional i32 os_api,
    5: optional bool is_jailbroken,
    6: optional string carrier,
    7: optional string idfa,
    8: optional string rom,
    9: required base.Base Base,
}

struct UpdateDeviceFullRequest {
    1: required i64 did,
    2: optional string clientudid,
    3: optional string os_version,
    4: optional i32 os_api,
    5: optional bool is_jailbroken,
    6: optional string carrier,
    7: optional string idfa,
    8: optional string rom,
    9: optional string os,
    10: optional string model,
    11: optional string resolution,
    12: optional string display_density,
    13: optional string mc (go.tag = "json:\"mc,sensitive\""),
    14: optional string language,
    15: optional string region,
    16: optional string tz_name,
    17: optional i32 tz_offset,
    18: optional string udid (go.tag = "json:\"udid,sensitive\""),
    19: optional string openudid,
    20: optional string build_serial,
    21: optional string pc_uuid,
    22: optional string pc_serial,
    23: optional string macos_uuid,
    24: optional string macos_serial,
    25: optional i32 cheat_code,
    26: optional string oaid,
    27: optional string device_brand,
    28: optional string drm_id,
    29: optional string drm_provider,
    30: optional bool is_track_limited,
    31: optional string google_aid,

    // 32~38: iOS global did, [doc link](https://bytedance.feishu.cn/docs/doccn7XY6TzHhvBDrgisugza9Pc)
    // Services beyond UCenter/UCenter/Device, will recieve empty fields below.
    32: optional string local_tz_name,
    33: optional string startup_time,
    34: optional string mb_time,
    35: optional string phone_name,
    36: optional string cpu_num,
    37: optional string disk_total,
    38: optional string mem_total,
    39: optional i32 auth_status,

    40: optional string tizen_duid, // https://developer.samsung.com/smarttv/develop/api-references/samsung-product-api-references/productinfo-api.html#::ProductInfo::ProductInfoManager::getDuid
    41: optional string tizen_uuid, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/uuid.html
    42: optional string tizen_tifa, // https://developer.samsung.com/smarttv/develop/guides/unique-identifiers-for-smarttv/tizen-id-for-advertising.html

    43: optional string build_time,
    44: optional string hardware_model,
    45: optional string locale_language,
    46: optional string ct_uuid,

    47: optional string webos_uuid,

    48: optional string app_trait, 
    49: optional bool is_harmony,  // Set true if it belongs to Huawei Harmony OS.
    50: optional string pico_sn,

    255: required base.Base Base,
}


struct DeviceResponse {
    1: Device device,
    3: optional string hit_rule,//hit_rule取值范围:https://code.byted.org/device/common_lib/blob/master/common_consts/device_rule.go
    2: base.BaseResp BaseResp,
}

struct GetOrCreateDeviceResponse {
    1: Device device,
    2: bool created,
    4: optional string hit_rule,//hit_rule取值范围:https://code.byted.org/device/common_lib/blob/master/common_consts/device_rule.go
    3: base.BaseResp BaseResp,
}

enum NoticeStatusType{
    ENABLED = 0,
    DISABLED = 1,
}

struct Installation {
    1: i64 id=0,
    2: i64 did,
    3: i64 from_id=0,
    4: string app_key,
    5: string app_name,
    6: string display_app_name,
    7: string app_version,
    8: string token,
    9: NoticeStatusType notice_status=NoticeStatusType.ENABLED,
    10: string sdk_version,
    11: string package,
    12: string channel,
    13: i32 version_code,
    14: i64 create_time=0,
    15: i64 last_time=0,
    16: i64 activate_time=0,
    17: i64 modify_time=0,
    18: i64 first_time=0,
    19: bool is_paid_user=false,
    20: string first_channel,
    21: string ip,
    22: list<i32> push_sdk,
    23: list<i32> push_os,
    // wiki: https://wiki.bytedance.net/pages/viewpage.action?pageId=77049321
    // -1: 异常值, 0: 未启动, >0 有启动。启动记录不包括当前日期
    24: i32 launch_history,
    25: string language,
    26: string region,
    27: optional i32 cheat_code,
    28: optional string extra, // 目前仅限响应，请求带此字段无用
    29: optional i32 app_id, // 本字段虽为optinal，但在Get接口会和app_name一同返回

    // 以下三个字段为预装app字段，其他app不做保证
    30: optional string pre_installed_channel // 预装channel
    31: optional i64 apk_first_install_time // 应用安装时间戳
    32: optional i32 is_system_app // 是否为系统应用

    33: optional InstallationCode installation_code // 安装code

    // 内部使用，表示first_time目前来自于FirstInstallation的create_time，后续可能会更新
    34: optional bool is_first_time_unstable

    35: optional i64 reinstallation_time // 卸载重装时间
}

struct GetInstallationRequest {
    1: required i64 iid,
    2: required base.Base Base,
}

struct GetLatestInstallationRequest {
    1: required i64 did,
    2: required string package,
    3: required base.Base Base,
}

struct GetInstallationByProductIdRequest {
    1: required i64 did,
    2: required i32 product_id,
    3: required base.Base Base,
}

struct GetInstallationByAppIdRequest {
    1: required i64 did,
    2: required i32 app_id,
    3: required base.Base Base,
}

struct GetOrCreateInstallationRequest {
    1: required Installation installation,
    2: optional string ip, // client user ip
    3: optional string user_agent,
    4: optional bool is_replaced_register,
    32: required base.Base Base,
}

struct CreateInstallationRequest {
    1: required Installation installation,
    2: optional string ip, // client user ip
    3: optional string user_agent,
    4: optional bool is_replaced_register,
    255: required base.Base Base,
}

struct UpdateInstallationActivateTimeRequest {
    1: required i64 iid,
    2: required i64 activate_time,
    3: required base.Base Base,
}

struct UpdateInstallationLastTimeRequest {
    1: required i64 iid,
    2: required i64 last_time,
    3: required base.Base Base,
}

struct UpdateInstallationLanguageRegionRequest {
    1: required i64 iid,
    2: optional string language,
    3: optional string region,
    4: required base.Base Base,
}

struct UpdateInstallationRequest{
    1: required i64 iid,
    2: optional i32 cheat_code,
    3: optional string language,
    4: optional string region,
    5: optional string pre_installed_channel,
    6: optional i64 reinstallation_time,
    255:required base.Base Base,
}

struct InstallationResponse {
    1: Installation installation,
    2: base.BaseResp BaseResp
}

struct FullInstallationResponse {
    1: Device device,
    2: Installation installation,
    3: base.BaseResp BaseResp
}

struct GetOrCreateInstallationResponse {
    1: Installation installation,
    2: bool created,
    32: base.BaseResp BaseResp
}

struct CreateInstallationResponse {
    1: optional Installation installation,
    2: optional bool created,
    255: base.BaseResp BaseResp
}

struct DeviceToken {
    1: i64 id=0,
    2: i64 device_id,
    3: i32 app_id,
    4: i32 token_type,
    5: string token_value,
    6: bool token_enabled=true,
    7: bool token_debug_enabled=true,
    8: i32 create_time,
    9: i32 modify_time,
}

struct DeviceTokenResponse {
    1: DeviceToken token,
    2: base.BaseResp BaseResp,
}

struct DeviceUninstall {
    1: i64 id=0,
    2: i64 user_id,
    3: i64 device_id,
    4: i32 app_id,
    5: i32 app_version,
    6: string app_platform,
    7: string token,
    8: string language,
    9: string region,
    10: i32 status=1,
    11: i32 create_time,
    12: i32 accuracy,
}

struct PendingUninstallTrack {
    1: i64 id,
    2: i64 user_id,
    3: i64 device_id,
    4: i32 app_id,
    5: i32 app_version,
    6: string app_platform,
    7: string token,
    8: bool token_enabled=true,
    9: string language,
    10: string region,
    11: i32 status=1,
    12: i32 partition,
    13: i32 create_time,
    14: i32 modify_time,
}

struct ProductApp {
    1: i32 app_id,
    2: string app_name,
    3: string app_cn_name,
    4: i32 product_id,
    5: string product_name,
    6: i32 client_id,
    7: string android_package,
    8: string iphone_package,
    9: string ipad_package,
    10: string winphone_package,
    11: i32 android_member_id,
    12: i32 iphone_member_id,
    13: i32 ipad_member_id,
    14: string extra
    15: i64 create_time,
    16: i64 modify_time,
    17: i32 status=1,
    18: string app_store_url,
    19: string weixin_url,
    20: string tt_app_key,
    21: string app_type,
    22: i32 is_external,
    23: string description,
}

struct GetProductAppByMarksRequest {
    1: optional i32 app_id,
    2: optional string app_name,
    3: optional string app_cn_name,
    4: optional string android_package,
    5: optional string iphone_package,
    6: optional string ipad_package,
    7: optional string winphone_package,
    8: optional string tt_app_key,
    32: required base.Base Base,
}

struct ProductAppResponse {
    1: ProductApp product_app,
    32: base.BaseResp BaseResp,
}

struct Product {
    1: i32 product_id,
    2: string product_name,
    3: string product_cn_name,
    4: i64 create_time,
    5: i64 modify_time,
    6: i32 status=1,
}

struct UpdateSpecificChannelRequest {
    1: required i64 iid,
    2: required string specific_channel
    32: required base.Base Base,
}

struct UpdateSpecificChannelResponse  {
    1:  bool is_success
    32: base.BaseResp BaseResp
}

struct GetSpecificChannelRequest {
    1: required i64 iid,
    32: required base.Base Base,
}

struct GetSpecificChannelResponse  {
    1:  string specific_channel
    32: base.BaseResp BaseResp
}

struct MGetFullInstallsRequest {
    1: required list<i64> iids,
    8: required base.Base Base,
}

struct MGetDevicesRequest {
    1: required list<i64> dids,
    8: required base.Base Base,
}

struct MGetResponse  {
    1: list<i64> fail_ids
    2: list<Installation> installs
    3: list<Device> devices
    8: base.BaseResp BaseResp
}


service DeviceService {
    DeviceResponse GetDevice(1:GetDeviceRequest request)
    // 功能同GetDeviceByMarks，但是GetDeviceByMarks入参因为框架要求Request最后一位必须是Base，
    // 因此不能再加新参数(google_aid,build_serial)，所以新加一个接口
    DeviceResponse GetDeviceByIdentifiers(1:GetDeviceByIdentifiersRequest request)
    GetOrCreateDeviceResponse GetOrCreateDevice(1:CASDeviceRequest request)
    DeviceResponse CreateDevice(1:CASDeviceRequest request)
    DeviceResponse UpdateDevice(1:UpdateDeviceRequest request)
    DeviceResponse UpdateDeviceFull(1:UpdateDeviceFullRequest request)

    InstallationResponse GetInstallation(1:GetInstallationRequest request)
    FullInstallationResponse GetFullInstallation(1:GetInstallationRequest request)
    InstallationResponse GetLatestInstallation(1:GetLatestInstallationRequest request)
    InstallationResponse GetFirstInstallationByProductId(1:GetInstallationByProductIdRequest request)
    InstallationResponse GetLatestInstallationByAppId(1:GetInstallationByAppIdRequest request)
    InstallationResponse GetFirstInstallationByAppId(1:GetInstallationByAppIdRequest request)
    GetOrCreateInstallationResponse GetOrCreateInstallation(1:GetOrCreateInstallationRequest request)
    CreateInstallationResponse CreateInstallation(1:CreateInstallationRequest request)
    InstallationResponse UpdateInstallationActivateTime(1:UpdateInstallationActivateTimeRequest request)
    InstallationResponse UpdateInstallationLastTime(1:UpdateInstallationLastTimeRequest request)
    InstallationResponse UpdateInstallationLanguageRegion(1:UpdateInstallationLanguageRegionRequest request)
    InstallationResponse UpdateInstallation(1:UpdateInstallationRequest request)

    // 批量获取安装与设备信息
    MGetResponse MGetFullInstalls(1:MGetFullInstallsRequest request)
    MGetResponse MGetDevices(1:MGetDevicesRequest request)

    // SpecificChannel api
    GetSpecificChannelResponse GetSpecificChannel(1:GetSpecificChannelRequest request)
    UpdateSpecificChannelResponse UpdateSpecificChannel(1:UpdateSpecificChannelRequest request)

    // drapcated soon
    DeviceResponse GetDeviceByMarks(1:GetDeviceByMarksRequest request) // WARNING: will been deprecated soon

    /********** the following api are deprecated **********/
    ProductAppResponse GetProductAppByMarks(1:GetProductAppByMarksRequest request)
}
