namespace go common_param
namespace py common_param

struct Session {
    1: i64 UserId                                       (go.tag = 'json:\"user_id\"')
}

struct OdinInfo {
	1: i64 Uid                                          (go.tag = 'json:\"uid\"')
	2: i32 UidType                                      (go.tag = 'json:\"uid_type\"')
	3: i32 UserIsAuth                                   (go.tag = 'json:\"user_is_auth\"')
	4: i64 RegisterTime                                 (go.tag = 'json:\"register_time\"')
}

struct DidCheckResult {
    1: bool DidIllegal                                  (go.tag = 'json:\"did_illegal\"')
}

struct UserInfo {
	1: string Name                                      (go.tag = 'json:\"name\"')
	2: string AvatarUrl                                 (go.tag = 'json:\"avatar_url\"')
	3: i64 SinaUserId                                   (go.tag = 'json:\"sina_user_id\"')
	4: i64 TencentUserId                                (go.tag = 'json:\"tencent_user_id\"')
	5: i64 UserRegisterTime                             (go.tag = 'json:\"user_register_time\"')
	6: i64 MediaId                                      (go.tag = 'json:\"media_id\"')
	7: i64 UgcPublishMediaId                            (go.tag = 'json:\"ugc_publish_media_id\"')
	8: string UserAuthInfo                              (go.tag = 'json:\"user_auth_info\"')
	9: i32 UserAuthType                                 (go.tag = 'json:\"user_auth_type\"')
	10: i32 IsExpert                                    (go.tag = 'json:\"is_expert\"')
	11: i64 BanStatus                                   (go.tag = 'json:\"ban_status\"')
	12: string PunishStatus                             (go.tag = 'json:\"punish_status\"')
}

struct CaptchaTokenResult {
    1: string Token                                     (go.tag='json:\"token\"')
}

struct CaptchaCheckResult {
	1:  bool CaptchaIsValid                             (go.tag = 'json:\"captcha_is_valid\"')
	2:  bool CaptchaParamsCheck                         (go.tag = 'json:\"captcha_params_check\"')
	3:  i64  CaptchaEnvCheck                            (go.tag = 'json:\"captcha_env_check\"')
	4:  i64  CaptchaFeatureResults                      (go.tag = 'json:\"captcha_feature_results\"')
	5:  i64  CaptchaFeatureResultsMark                  (go.tag = 'json:\"captcha_feature_results_mark\"')
	6:  i64  CaptchaFeVersion                           (go.tag = 'json:\"captcha_fe_version\"')
	7:  i64  CaptchaFeatureVersion                      (go.tag = 'json:\"captcha_feature_version\"')
	8:  i64  CaptchaBackendVersion                      (go.tag = 'json:\"captcha_backend_version\"')
	9:  i64  CaptchaTimestamp                           (go.tag = 'json:\"captcha_timestamp\"')
	10: i64  CaptchaFingerprint                         (go.tag = 'json:\"captcha_fingerprint\"')
	11: i64  CaptchaHashVersion                         (go.tag = 'json:\"captcha_hash_version\"')
}

struct UnifyArgs {
    1: i32 PlatformId                                   (go.tag = 'json:\"platform_id\"')
    2: bool IsIos                                       (go.tag = 'json:\"is_ios\"')
    3: bool IsAndroid                                   (go.tag = 'json:\"is_android\"')
    4: i32 AccessType                                   (go.tag = 'json:\"access_type\"')
    5: i32 ResolutionWidth                              (go.tag = 'json:\"resolution_width\"')
    6: i32 ResolutionHeight                             (go.tag = 'json:\"resolution_height\"')
    7: i64 UnifyVersionCode60101                        (go.tag = 'json:\"unify_version_code\"')
    8: i64 UnifyVersionCode611                          (go.tag = 'json:\"unify_version_code_611\"')
    9: i32 ProductId                                    (go.tag = 'json:\"product_id\"')
    10: i64 Region                                      (go.tag = 'json:\"region\"')
    11: string AppCnName                                (go.tag = 'json:\"app_cn_name\"')
    12: string AppName                                  (go.tag = 'json:\"app_name\"')
    
    # 来自query里面的"aid"或者"app_id"
    100: i32 AppId                                      (go.tag = 'json:\"app_id\"')

    # 来自cookie里面的"install_id"或者query的"iid"
    101: i64 InstallId                                  (go.tag = 'json:\"install_id\"')

    # 来自query的"device_id", 或者是根据install_id从device_info服务获取的device_id
    102: i64 DeviceId                                   (go.tag = 'json:\"device_id\"')

    # 最接近用户的ip, 获取逻辑参考: https://wiki.bytedance.net/pages/viewpage.action?pageId=119674842
    103: string ClientIp                                (go.tag = 'json:\"client_ip\"')
}


# 直接对应同名的query参数,没有任何处理逻辑

struct CommonArgs {
	1: string AppName                                   (go.tag = 'json:\"app_name\"')
	2: string Channel                                   (go.tag = 'json:\"channel\"')
	3: string AppVersion                                (go.tag = 'json:\"app_version\"')
	4: string VersionCode                               (go.tag = 'json:\"version_code\"')
	5: string VersionName                               (go.tag = 'json:\"version_name\"')
	6: i64 UpdateVersionCode                            (go.tag = 'json:\"update_version_code\"')
	7: string Language                                  (go.tag = 'json:\"language\"')
	8: string OsVersion                                 (go.tag = 'json:\"os_version\"')
	9: i32 OsApi                                        (go.tag = 'json:\"os_api\"')
	10: string DeviceType                               (go.tag = 'json:\"device_type\"')
	11: string DeviceBrand                              (go.tag = 'json:\"device_brand\"')
	12: string DeviceModel                              (go.tag = 'json:\"device_model\"')
	13: string DevicePlatform                           (go.tag = 'json:\"device_platform\"')
	14: string Resolution                               (go.tag = 'json:\"resolution\"')
	15: i64 Plugin                                      (go.tag = 'json:\"plugin\"')
	16: string Ac                                       (go.tag = 'json:\"ac\"')
	17: string AbClient                                 (go.tag = 'json:\"ab_client\"')
	18: string AbVersion                                (go.tag = 'json:\"ab_version\"')
	19: string AbGroup                                  (go.tag = 'json:\"ab_group\"')
	20: string AbFeature                                (go.tag = 'json:\"ab_feature\"')
	21: string OpenUdid                                 (go.tag = 'json:\"openudid\"')
	22: string Idfa                                     (go.tag = 'json:\"idfa\"')
	23: string Uuid                                     (go.tag = 'json:\"uuid\"')
	24: string ClientDeviceId                           (go.tag = 'json:\"cdid\"')     # 用户设备注册前的 device_id，注册后可以与 DeviceId 映射，见：https://appcloud.bytedance.net/doc/315/18244/

}

struct BddidToDid {
    1: i64 DeviceId                                     (go.tag = 'json:\"device_id\"')
}

struct BlockDecisionConf {
    1: i32 StatusCode                                   (go.tag = 'json:\"statuc_code\"')
    2: string Content                                   (go.tag = 'json:\"content\"')
    3: string ContentType                               (go.tag = 'json:\"content_type\"')
    4: map<string,string> CustomHeaders                 (go.tag = 'json:\"custom_headers\"')
}

struct MockDecisionConf {
    1: string Url                                       (go.tag = 'json:\"url\"')
    2: bool WithOriginal                                (go.tag = 'json:\"with_original\"')
}

struct WhaleResult {
    1: string Decision                                  (go.tag = 'json:\"decision\"')
    2: string CustomConf                                (go.tag = 'json:\"custom_conf\"')
    3: optional BlockDecisionConf BlockConf             (go.tag = 'json:\"block_conf\"')
    4: optional MockDecisionConf MockConf               (go.tag = 'json:\"mock_conf\"')
}
