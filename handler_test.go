package main

import (
	"code.byted.org/gopkg/logs"
	"code.byted.org/libo.1220/libo.1220_api/clients"
	"code.byted.org/libo.1220/libo.1220_api/kitex_gen/common_param"
	"code.byted.org/libo.1220/libo.1220_api/kitex_gen/motor/ad/libo_api"
	"context"
	"testing"
)

func TestDemoApiServiceImpl_SayHi(t *testing.T) {
	ctx := context.Background()
	logs.CtxDebug(ctx, "SayHi called")
	req := &libo_api.DemoApiReq{}
	req.AgwCommonParam = &libo_api.AgwCommonParam{
		Session: &common_param.Session{UserId: 123},
	}
	req.SetRawUri("/libo.1220_api/demo_api/test/say-hi/")
	resp, err := clients.DemoApiClient.SayHi(ctx, req)
	if resp == nil || err != nil {
		logs.CtxError(ctx, "SayHi failed: %+v, resp is %+v", err, resp)
		t.FailNow()
	}
	logs.CtxInfo(ctx, "resp is %+v", string(resp.GetBody()))

}
