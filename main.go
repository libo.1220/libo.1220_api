package main

import (
	libo_api "code.byted.org/libo.1220/libo.1220_api/kitex_gen/motor/ad/libo_api/demoapiservice"
	"log"
)

func main() {
	svr := libo_api.NewServer(new(DemoApiServiceImpl))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
