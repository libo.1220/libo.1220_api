package main

import (
	"code.byted.org/gopkg/logs"
	"code.byted.org/libo.1220/libo.1220_api/kitex_gen/motor/ad/libo_api"
	"context"
	"fmt"
)

// DemoApiServiceImpl implements the last service interface defined in the IDL.
type DemoApiServiceImpl struct{}

// SayHi implements the DemoApiServiceImpl interface.
func (s *DemoApiServiceImpl) SayHi(ctx context.Context, request *libo_api.DemoApiReq) (resp *libo_api.DemoApiResp, err error) {
	// TODO: Your code here...
	logs.CtxInfo(ctx, "SayHi request: %+v", request)
	resp = libo_api.NewDemoApiResp()
	resp.SetBody([]byte(fmt.Sprint("Hello world!")))
	return
}
