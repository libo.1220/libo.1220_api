package clients

import (
	"code.byted.org/kite/kitex/client"
	"code.byted.org/libo.1220/libo.1220_api/kitex_gen/motor/ad/libo_api/demoapiservice"
)

var DemoApiClient demoapiservice.Client

func init() {
	var err error
	DemoApiClient, err = demoapiservice.NewClient("motor/ad/libo_api",
		client.WithHostPorts("127.0.0.1:8888"))
	if err != nil {
		panic(err)
	}
}
