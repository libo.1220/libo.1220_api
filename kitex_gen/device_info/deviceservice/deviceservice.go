// Code generated by Kitex v1.9.3. DO NOT EDIT.

package deviceservice

import (
	"code.byted.org/kite/kitex/client"
	kitex "code.byted.org/kite/kitex/pkg/serviceinfo"
	"code.byted.org/libo.1220/libo.1220_api/kitex_gen/device_info"
	"context"
)

func serviceInfo() *kitex.ServiceInfo {
	return deviceServiceServiceInfo
}

var deviceServiceServiceInfo = newServiceInfo()

func newServiceInfo() *kitex.ServiceInfo {
	serviceName := "DeviceService"
	handlerType := (*device_info.DeviceService)(nil)
	methods := map[string]kitex.MethodInfo{
		"GetDevice":                        kitex.NewMethodInfo(getDeviceHandler, newDeviceServiceGetDeviceArgs, newDeviceServiceGetDeviceResult, false),
		"GetDeviceByIdentifiers":           kitex.NewMethodInfo(getDeviceByIdentifiersHandler, newDeviceServiceGetDeviceByIdentifiersArgs, newDeviceServiceGetDeviceByIdentifiersResult, false),
		"GetOrCreateDevice":                kitex.NewMethodInfo(getOrCreateDeviceHandler, newDeviceServiceGetOrCreateDeviceArgs, newDeviceServiceGetOrCreateDeviceResult, false),
		"CreateDevice":                     kitex.NewMethodInfo(createDeviceHandler, newDeviceServiceCreateDeviceArgs, newDeviceServiceCreateDeviceResult, false),
		"UpdateDevice":                     kitex.NewMethodInfo(updateDeviceHandler, newDeviceServiceUpdateDeviceArgs, newDeviceServiceUpdateDeviceResult, false),
		"UpdateDeviceFull":                 kitex.NewMethodInfo(updateDeviceFullHandler, newDeviceServiceUpdateDeviceFullArgs, newDeviceServiceUpdateDeviceFullResult, false),
		"GetInstallation":                  kitex.NewMethodInfo(getInstallationHandler, newDeviceServiceGetInstallationArgs, newDeviceServiceGetInstallationResult, false),
		"GetFullInstallation":              kitex.NewMethodInfo(getFullInstallationHandler, newDeviceServiceGetFullInstallationArgs, newDeviceServiceGetFullInstallationResult, false),
		"GetLatestInstallation":            kitex.NewMethodInfo(getLatestInstallationHandler, newDeviceServiceGetLatestInstallationArgs, newDeviceServiceGetLatestInstallationResult, false),
		"GetFirstInstallationByProductId":  kitex.NewMethodInfo(getFirstInstallationByProductIdHandler, newDeviceServiceGetFirstInstallationByProductIdArgs, newDeviceServiceGetFirstInstallationByProductIdResult, false),
		"GetLatestInstallationByAppId":     kitex.NewMethodInfo(getLatestInstallationByAppIdHandler, newDeviceServiceGetLatestInstallationByAppIdArgs, newDeviceServiceGetLatestInstallationByAppIdResult, false),
		"GetFirstInstallationByAppId":      kitex.NewMethodInfo(getFirstInstallationByAppIdHandler, newDeviceServiceGetFirstInstallationByAppIdArgs, newDeviceServiceGetFirstInstallationByAppIdResult, false),
		"GetOrCreateInstallation":          kitex.NewMethodInfo(getOrCreateInstallationHandler, newDeviceServiceGetOrCreateInstallationArgs, newDeviceServiceGetOrCreateInstallationResult, false),
		"CreateInstallation":               kitex.NewMethodInfo(createInstallationHandler, newDeviceServiceCreateInstallationArgs, newDeviceServiceCreateInstallationResult, false),
		"UpdateInstallationActivateTime":   kitex.NewMethodInfo(updateInstallationActivateTimeHandler, newDeviceServiceUpdateInstallationActivateTimeArgs, newDeviceServiceUpdateInstallationActivateTimeResult, false),
		"UpdateInstallationLastTime":       kitex.NewMethodInfo(updateInstallationLastTimeHandler, newDeviceServiceUpdateInstallationLastTimeArgs, newDeviceServiceUpdateInstallationLastTimeResult, false),
		"UpdateInstallationLanguageRegion": kitex.NewMethodInfo(updateInstallationLanguageRegionHandler, newDeviceServiceUpdateInstallationLanguageRegionArgs, newDeviceServiceUpdateInstallationLanguageRegionResult, false),
		"UpdateInstallation":               kitex.NewMethodInfo(updateInstallationHandler, newDeviceServiceUpdateInstallationArgs, newDeviceServiceUpdateInstallationResult, false),
		"MGetFullInstalls":                 kitex.NewMethodInfo(mGetFullInstallsHandler, newDeviceServiceMGetFullInstallsArgs, newDeviceServiceMGetFullInstallsResult, false),
		"MGetDevices":                      kitex.NewMethodInfo(mGetDevicesHandler, newDeviceServiceMGetDevicesArgs, newDeviceServiceMGetDevicesResult, false),
		"GetSpecificChannel":               kitex.NewMethodInfo(getSpecificChannelHandler, newDeviceServiceGetSpecificChannelArgs, newDeviceServiceGetSpecificChannelResult, false),
		"UpdateSpecificChannel":            kitex.NewMethodInfo(updateSpecificChannelHandler, newDeviceServiceUpdateSpecificChannelArgs, newDeviceServiceUpdateSpecificChannelResult, false),
		"GetDeviceByMarks":                 kitex.NewMethodInfo(getDeviceByMarksHandler, newDeviceServiceGetDeviceByMarksArgs, newDeviceServiceGetDeviceByMarksResult, false),
		"GetProductAppByMarks":             kitex.NewMethodInfo(getProductAppByMarksHandler, newDeviceServiceGetProductAppByMarksArgs, newDeviceServiceGetProductAppByMarksResult, false),
	}
	extra := map[string]interface{}{
		"PackageName": "device_info",
	}
	svcInfo := &kitex.ServiceInfo{
		ServiceName:     serviceName,
		HandlerType:     handlerType,
		Methods:         methods,
		PayloadCodec:    kitex.Thrift,
		KiteXGenVersion: "v1.9.3",
		Extra:           extra,
	}
	return svcInfo
}

func getDeviceHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetDeviceArgs)
	realResult := result.(*device_info.DeviceServiceGetDeviceResult)
	success, err := handler.(device_info.DeviceService).GetDevice(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetDeviceArgs() interface{} {
	return device_info.NewDeviceServiceGetDeviceArgs()
}

func newDeviceServiceGetDeviceResult() interface{} {
	return device_info.NewDeviceServiceGetDeviceResult()
}

func getDeviceByIdentifiersHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetDeviceByIdentifiersArgs)
	realResult := result.(*device_info.DeviceServiceGetDeviceByIdentifiersResult)
	success, err := handler.(device_info.DeviceService).GetDeviceByIdentifiers(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetDeviceByIdentifiersArgs() interface{} {
	return device_info.NewDeviceServiceGetDeviceByIdentifiersArgs()
}

func newDeviceServiceGetDeviceByIdentifiersResult() interface{} {
	return device_info.NewDeviceServiceGetDeviceByIdentifiersResult()
}

func getOrCreateDeviceHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetOrCreateDeviceArgs)
	realResult := result.(*device_info.DeviceServiceGetOrCreateDeviceResult)
	success, err := handler.(device_info.DeviceService).GetOrCreateDevice(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetOrCreateDeviceArgs() interface{} {
	return device_info.NewDeviceServiceGetOrCreateDeviceArgs()
}

func newDeviceServiceGetOrCreateDeviceResult() interface{} {
	return device_info.NewDeviceServiceGetOrCreateDeviceResult()
}

func createDeviceHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceCreateDeviceArgs)
	realResult := result.(*device_info.DeviceServiceCreateDeviceResult)
	success, err := handler.(device_info.DeviceService).CreateDevice(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceCreateDeviceArgs() interface{} {
	return device_info.NewDeviceServiceCreateDeviceArgs()
}

func newDeviceServiceCreateDeviceResult() interface{} {
	return device_info.NewDeviceServiceCreateDeviceResult()
}

func updateDeviceHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateDeviceArgs)
	realResult := result.(*device_info.DeviceServiceUpdateDeviceResult)
	success, err := handler.(device_info.DeviceService).UpdateDevice(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateDeviceArgs() interface{} {
	return device_info.NewDeviceServiceUpdateDeviceArgs()
}

func newDeviceServiceUpdateDeviceResult() interface{} {
	return device_info.NewDeviceServiceUpdateDeviceResult()
}

func updateDeviceFullHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateDeviceFullArgs)
	realResult := result.(*device_info.DeviceServiceUpdateDeviceFullResult)
	success, err := handler.(device_info.DeviceService).UpdateDeviceFull(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateDeviceFullArgs() interface{} {
	return device_info.NewDeviceServiceUpdateDeviceFullArgs()
}

func newDeviceServiceUpdateDeviceFullResult() interface{} {
	return device_info.NewDeviceServiceUpdateDeviceFullResult()
}

func getInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetInstallationArgs)
	realResult := result.(*device_info.DeviceServiceGetInstallationResult)
	success, err := handler.(device_info.DeviceService).GetInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetInstallationArgs() interface{} {
	return device_info.NewDeviceServiceGetInstallationArgs()
}

func newDeviceServiceGetInstallationResult() interface{} {
	return device_info.NewDeviceServiceGetInstallationResult()
}

func getFullInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetFullInstallationArgs)
	realResult := result.(*device_info.DeviceServiceGetFullInstallationResult)
	success, err := handler.(device_info.DeviceService).GetFullInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetFullInstallationArgs() interface{} {
	return device_info.NewDeviceServiceGetFullInstallationArgs()
}

func newDeviceServiceGetFullInstallationResult() interface{} {
	return device_info.NewDeviceServiceGetFullInstallationResult()
}

func getLatestInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetLatestInstallationArgs)
	realResult := result.(*device_info.DeviceServiceGetLatestInstallationResult)
	success, err := handler.(device_info.DeviceService).GetLatestInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetLatestInstallationArgs() interface{} {
	return device_info.NewDeviceServiceGetLatestInstallationArgs()
}

func newDeviceServiceGetLatestInstallationResult() interface{} {
	return device_info.NewDeviceServiceGetLatestInstallationResult()
}

func getFirstInstallationByProductIdHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetFirstInstallationByProductIdArgs)
	realResult := result.(*device_info.DeviceServiceGetFirstInstallationByProductIdResult)
	success, err := handler.(device_info.DeviceService).GetFirstInstallationByProductId(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetFirstInstallationByProductIdArgs() interface{} {
	return device_info.NewDeviceServiceGetFirstInstallationByProductIdArgs()
}

func newDeviceServiceGetFirstInstallationByProductIdResult() interface{} {
	return device_info.NewDeviceServiceGetFirstInstallationByProductIdResult()
}

func getLatestInstallationByAppIdHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetLatestInstallationByAppIdArgs)
	realResult := result.(*device_info.DeviceServiceGetLatestInstallationByAppIdResult)
	success, err := handler.(device_info.DeviceService).GetLatestInstallationByAppId(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetLatestInstallationByAppIdArgs() interface{} {
	return device_info.NewDeviceServiceGetLatestInstallationByAppIdArgs()
}

func newDeviceServiceGetLatestInstallationByAppIdResult() interface{} {
	return device_info.NewDeviceServiceGetLatestInstallationByAppIdResult()
}

func getFirstInstallationByAppIdHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetFirstInstallationByAppIdArgs)
	realResult := result.(*device_info.DeviceServiceGetFirstInstallationByAppIdResult)
	success, err := handler.(device_info.DeviceService).GetFirstInstallationByAppId(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetFirstInstallationByAppIdArgs() interface{} {
	return device_info.NewDeviceServiceGetFirstInstallationByAppIdArgs()
}

func newDeviceServiceGetFirstInstallationByAppIdResult() interface{} {
	return device_info.NewDeviceServiceGetFirstInstallationByAppIdResult()
}

func getOrCreateInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetOrCreateInstallationArgs)
	realResult := result.(*device_info.DeviceServiceGetOrCreateInstallationResult)
	success, err := handler.(device_info.DeviceService).GetOrCreateInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetOrCreateInstallationArgs() interface{} {
	return device_info.NewDeviceServiceGetOrCreateInstallationArgs()
}

func newDeviceServiceGetOrCreateInstallationResult() interface{} {
	return device_info.NewDeviceServiceGetOrCreateInstallationResult()
}

func createInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceCreateInstallationArgs)
	realResult := result.(*device_info.DeviceServiceCreateInstallationResult)
	success, err := handler.(device_info.DeviceService).CreateInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceCreateInstallationArgs() interface{} {
	return device_info.NewDeviceServiceCreateInstallationArgs()
}

func newDeviceServiceCreateInstallationResult() interface{} {
	return device_info.NewDeviceServiceCreateInstallationResult()
}

func updateInstallationActivateTimeHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateInstallationActivateTimeArgs)
	realResult := result.(*device_info.DeviceServiceUpdateInstallationActivateTimeResult)
	success, err := handler.(device_info.DeviceService).UpdateInstallationActivateTime(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateInstallationActivateTimeArgs() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationActivateTimeArgs()
}

func newDeviceServiceUpdateInstallationActivateTimeResult() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationActivateTimeResult()
}

func updateInstallationLastTimeHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateInstallationLastTimeArgs)
	realResult := result.(*device_info.DeviceServiceUpdateInstallationLastTimeResult)
	success, err := handler.(device_info.DeviceService).UpdateInstallationLastTime(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateInstallationLastTimeArgs() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationLastTimeArgs()
}

func newDeviceServiceUpdateInstallationLastTimeResult() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationLastTimeResult()
}

func updateInstallationLanguageRegionHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateInstallationLanguageRegionArgs)
	realResult := result.(*device_info.DeviceServiceUpdateInstallationLanguageRegionResult)
	success, err := handler.(device_info.DeviceService).UpdateInstallationLanguageRegion(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateInstallationLanguageRegionArgs() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationLanguageRegionArgs()
}

func newDeviceServiceUpdateInstallationLanguageRegionResult() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationLanguageRegionResult()
}

func updateInstallationHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateInstallationArgs)
	realResult := result.(*device_info.DeviceServiceUpdateInstallationResult)
	success, err := handler.(device_info.DeviceService).UpdateInstallation(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateInstallationArgs() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationArgs()
}

func newDeviceServiceUpdateInstallationResult() interface{} {
	return device_info.NewDeviceServiceUpdateInstallationResult()
}

func mGetFullInstallsHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceMGetFullInstallsArgs)
	realResult := result.(*device_info.DeviceServiceMGetFullInstallsResult)
	success, err := handler.(device_info.DeviceService).MGetFullInstalls(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceMGetFullInstallsArgs() interface{} {
	return device_info.NewDeviceServiceMGetFullInstallsArgs()
}

func newDeviceServiceMGetFullInstallsResult() interface{} {
	return device_info.NewDeviceServiceMGetFullInstallsResult()
}

func mGetDevicesHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceMGetDevicesArgs)
	realResult := result.(*device_info.DeviceServiceMGetDevicesResult)
	success, err := handler.(device_info.DeviceService).MGetDevices(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceMGetDevicesArgs() interface{} {
	return device_info.NewDeviceServiceMGetDevicesArgs()
}

func newDeviceServiceMGetDevicesResult() interface{} {
	return device_info.NewDeviceServiceMGetDevicesResult()
}

func getSpecificChannelHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetSpecificChannelArgs)
	realResult := result.(*device_info.DeviceServiceGetSpecificChannelResult)
	success, err := handler.(device_info.DeviceService).GetSpecificChannel(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetSpecificChannelArgs() interface{} {
	return device_info.NewDeviceServiceGetSpecificChannelArgs()
}

func newDeviceServiceGetSpecificChannelResult() interface{} {
	return device_info.NewDeviceServiceGetSpecificChannelResult()
}

func updateSpecificChannelHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceUpdateSpecificChannelArgs)
	realResult := result.(*device_info.DeviceServiceUpdateSpecificChannelResult)
	success, err := handler.(device_info.DeviceService).UpdateSpecificChannel(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceUpdateSpecificChannelArgs() interface{} {
	return device_info.NewDeviceServiceUpdateSpecificChannelArgs()
}

func newDeviceServiceUpdateSpecificChannelResult() interface{} {
	return device_info.NewDeviceServiceUpdateSpecificChannelResult()
}

func getDeviceByMarksHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetDeviceByMarksArgs)
	realResult := result.(*device_info.DeviceServiceGetDeviceByMarksResult)
	success, err := handler.(device_info.DeviceService).GetDeviceByMarks(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetDeviceByMarksArgs() interface{} {
	return device_info.NewDeviceServiceGetDeviceByMarksArgs()
}

func newDeviceServiceGetDeviceByMarksResult() interface{} {
	return device_info.NewDeviceServiceGetDeviceByMarksResult()
}

func getProductAppByMarksHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*device_info.DeviceServiceGetProductAppByMarksArgs)
	realResult := result.(*device_info.DeviceServiceGetProductAppByMarksResult)
	success, err := handler.(device_info.DeviceService).GetProductAppByMarks(ctx, realArg.Request)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newDeviceServiceGetProductAppByMarksArgs() interface{} {
	return device_info.NewDeviceServiceGetProductAppByMarksArgs()
}

func newDeviceServiceGetProductAppByMarksResult() interface{} {
	return device_info.NewDeviceServiceGetProductAppByMarksResult()
}

type kClient struct {
	c client.Client
}

func newServiceClient(c client.Client) *kClient {
	return &kClient{
		c: c,
	}
}

func (p *kClient) GetDevice(ctx context.Context, request *device_info.GetDeviceRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceGetDeviceArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetDeviceResult
	if err = p.c.Call(ctx, "GetDevice", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetDeviceByIdentifiers(ctx context.Context, request *device_info.GetDeviceByIdentifiersRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceGetDeviceByIdentifiersArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetDeviceByIdentifiersResult
	if err = p.c.Call(ctx, "GetDeviceByIdentifiers", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetOrCreateDevice(ctx context.Context, request *device_info.CASDeviceRequest) (r *device_info.GetOrCreateDeviceResponse, err error) {
	var _args device_info.DeviceServiceGetOrCreateDeviceArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetOrCreateDeviceResult
	if err = p.c.Call(ctx, "GetOrCreateDevice", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) CreateDevice(ctx context.Context, request *device_info.CASDeviceRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceCreateDeviceArgs
	_args.Request = request
	var _result device_info.DeviceServiceCreateDeviceResult
	if err = p.c.Call(ctx, "CreateDevice", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateDevice(ctx context.Context, request *device_info.UpdateDeviceRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceUpdateDeviceArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateDeviceResult
	if err = p.c.Call(ctx, "UpdateDevice", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateDeviceFull(ctx context.Context, request *device_info.UpdateDeviceFullRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceUpdateDeviceFullArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateDeviceFullResult
	if err = p.c.Call(ctx, "UpdateDeviceFull", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetInstallation(ctx context.Context, request *device_info.GetInstallationRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceGetInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetInstallationResult
	if err = p.c.Call(ctx, "GetInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetFullInstallation(ctx context.Context, request *device_info.GetInstallationRequest) (r *device_info.FullInstallationResponse, err error) {
	var _args device_info.DeviceServiceGetFullInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetFullInstallationResult
	if err = p.c.Call(ctx, "GetFullInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetLatestInstallation(ctx context.Context, request *device_info.GetLatestInstallationRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceGetLatestInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetLatestInstallationResult
	if err = p.c.Call(ctx, "GetLatestInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetFirstInstallationByProductId(ctx context.Context, request *device_info.GetInstallationByProductIdRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceGetFirstInstallationByProductIdArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetFirstInstallationByProductIdResult
	if err = p.c.Call(ctx, "GetFirstInstallationByProductId", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetLatestInstallationByAppId(ctx context.Context, request *device_info.GetInstallationByAppIdRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceGetLatestInstallationByAppIdArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetLatestInstallationByAppIdResult
	if err = p.c.Call(ctx, "GetLatestInstallationByAppId", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetFirstInstallationByAppId(ctx context.Context, request *device_info.GetInstallationByAppIdRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceGetFirstInstallationByAppIdArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetFirstInstallationByAppIdResult
	if err = p.c.Call(ctx, "GetFirstInstallationByAppId", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetOrCreateInstallation(ctx context.Context, request *device_info.GetOrCreateInstallationRequest) (r *device_info.GetOrCreateInstallationResponse, err error) {
	var _args device_info.DeviceServiceGetOrCreateInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetOrCreateInstallationResult
	if err = p.c.Call(ctx, "GetOrCreateInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) CreateInstallation(ctx context.Context, request *device_info.CreateInstallationRequest) (r *device_info.CreateInstallationResponse, err error) {
	var _args device_info.DeviceServiceCreateInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceCreateInstallationResult
	if err = p.c.Call(ctx, "CreateInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateInstallationActivateTime(ctx context.Context, request *device_info.UpdateInstallationActivateTimeRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceUpdateInstallationActivateTimeArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateInstallationActivateTimeResult
	if err = p.c.Call(ctx, "UpdateInstallationActivateTime", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateInstallationLastTime(ctx context.Context, request *device_info.UpdateInstallationLastTimeRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceUpdateInstallationLastTimeArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateInstallationLastTimeResult
	if err = p.c.Call(ctx, "UpdateInstallationLastTime", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateInstallationLanguageRegion(ctx context.Context, request *device_info.UpdateInstallationLanguageRegionRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceUpdateInstallationLanguageRegionArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateInstallationLanguageRegionResult
	if err = p.c.Call(ctx, "UpdateInstallationLanguageRegion", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateInstallation(ctx context.Context, request *device_info.UpdateInstallationRequest) (r *device_info.InstallationResponse, err error) {
	var _args device_info.DeviceServiceUpdateInstallationArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateInstallationResult
	if err = p.c.Call(ctx, "UpdateInstallation", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) MGetFullInstalls(ctx context.Context, request *device_info.MGetFullInstallsRequest) (r *device_info.MGetResponse, err error) {
	var _args device_info.DeviceServiceMGetFullInstallsArgs
	_args.Request = request
	var _result device_info.DeviceServiceMGetFullInstallsResult
	if err = p.c.Call(ctx, "MGetFullInstalls", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) MGetDevices(ctx context.Context, request *device_info.MGetDevicesRequest) (r *device_info.MGetResponse, err error) {
	var _args device_info.DeviceServiceMGetDevicesArgs
	_args.Request = request
	var _result device_info.DeviceServiceMGetDevicesResult
	if err = p.c.Call(ctx, "MGetDevices", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetSpecificChannel(ctx context.Context, request *device_info.GetSpecificChannelRequest) (r *device_info.GetSpecificChannelResponse, err error) {
	var _args device_info.DeviceServiceGetSpecificChannelArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetSpecificChannelResult
	if err = p.c.Call(ctx, "GetSpecificChannel", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateSpecificChannel(ctx context.Context, request *device_info.UpdateSpecificChannelRequest) (r *device_info.UpdateSpecificChannelResponse, err error) {
	var _args device_info.DeviceServiceUpdateSpecificChannelArgs
	_args.Request = request
	var _result device_info.DeviceServiceUpdateSpecificChannelResult
	if err = p.c.Call(ctx, "UpdateSpecificChannel", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetDeviceByMarks(ctx context.Context, request *device_info.GetDeviceByMarksRequest) (r *device_info.DeviceResponse, err error) {
	var _args device_info.DeviceServiceGetDeviceByMarksArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetDeviceByMarksResult
	if err = p.c.Call(ctx, "GetDeviceByMarks", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetProductAppByMarks(ctx context.Context, request *device_info.GetProductAppByMarksRequest) (r *device_info.ProductAppResponse, err error) {
	var _args device_info.DeviceServiceGetProductAppByMarksArgs
	_args.Request = request
	var _result device_info.DeviceServiceGetProductAppByMarksResult
	if err = p.c.Call(ctx, "GetProductAppByMarks", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}
