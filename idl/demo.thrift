namespace go motor.ad.libo_api   // 自行修改，后续不再提示
include "../service_rpc_idl/base.thrift"
include "../service_rpc_idl/api_gateway/agw_common_param.thrift"
include "../service_rpc_idl/device/device_info.thrift"

struct AgwCommonParam {
    1: agw_common_param.Session Session
    3: agw_common_param.UnifyArgs UnifyArgs
    4: agw_common_param.CommonArgs CommonArgs
    7: device_info.Device Device
}

struct DemoApiReq {
    1: binary RawBody (agw.source="raw_body")
    2: string RawUri (agw.source="raw_uri")
    5: map<string, list<string>> Headers (agw.source="headers")
    
    254: AgwCommonParam AgwCommonParam
    255: optional base.Base Base
}

struct DemoApiResp {    
    1: required binary Body (agw.target="body")

    255: optional base.BaseResp BaseResp
}

service DemoApiService {
    DemoApiResp SayHi (1:DemoApiReq request) (agw.method = 'GET', agw.uri 
= '/libo.1220_api/demo_api/test/say-hi/')
}
